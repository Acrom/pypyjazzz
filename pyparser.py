# coding=utf8

from lexer import tokens
from lexer import reserved
import ply.yacc as yacc


class Node:
	def parts_str(self):
		st = []
		for part in self.parts:
			st.append(str(part))
		return "\n".join(st)

	def __repr__(self):
		return self.type + ":\n\t" + self.parts_str().replace("\n", "\n\t")

	def add_parts(self, parts):
		self.parts += parts
		return self

	def shift_parts(self, parts):
		self.parts = parts + self.parts
		return self

	def __init__(self, m_type, parts):
		self.type = m_type
		self.parts = parts


def p_program(p):
	"""program : zero_block
			   | zero_block newline
			   | program zero_block
			   | program zero_block newline"""
	if len(p) == 4 or (len(p) == 3 and p[1].type == 'program'):
		p[0] = p[1].add_parts([p[2]])
	else:
		p[0] = Node('program', [p[1]])


def p_newline(p):
	"""newline : NEWLINE
				 | newline NEWLINE"""


def p_zero_block(p):
	"""zero_block : modification
				  | for_structure
				  | if_structure
				  | while_structure
				  | def_structure
				  | return_structure
				  | function"""
	if len(p) == 2:
		p[0] = p[1]


def p_modification(p):
	"""modification : IDENTIFIER ASSIGNMENT formula"""
	p[0] = Node('assignment', [Node(p[2], [p[1], p[3]])])


def p_atom(p):
	"""atom : IDENTIFIER
			| INTEGER_NUMBER
			| RATIONAL_NUMBER
			| STRING
			| BOOL
			| collection"""
	if p.slice[1].type == 'collection':
		p[0] = p[1]
	else:
		p[0] = Node(p.slice[1].type, [p[1]])


def p_formula(p):
	"""formula : t DIS formula
			   | t"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('or_structure', [p[1], p[3]])


def p_t(p):
	"""t : t CON f
		 | f"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('and_structure', [p[1], p[3]])


def p_f(p):
	"""f : f ADD g
		 | g"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('add', [Node(p[2], [p[1], p[3]])])


def p_g(p):
	"""g : g MUL h
		 | h"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('mul', [Node(p[2], [p[1], p[3]])])


def p_h(p):
	"""h : j DEG INTEGER_NUMBER
		 | j DEG RATIONAL_NUMBER
		 | j"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('deg', [Node(p[2], [p[1], Node(p.slice[3].type, [p[3]])])])


def p_j(p):
	"""j : LPAREN formula RPAREN
		 | atom
		 | function"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = Node('parents', [p[2]])


def p_reserved(p):
	"""reserved : PRINT
				| INT
				| FLOAT
				| STR
				| LEN
				| OBJECT
				| ABS
				| ROUND
				| CEIL
				| FLOOR
				| LIST
				| RANGE"""
	p[0] = p[1]


def p_function(p):
	"""function : IDENTIFIER LPAREN RPAREN
				| IDENTIFIER LPAREN cortege RPAREN
				| reserved LPAREN cortege RPAREN"""
	if len(p) == 4:
		p[0] = Node('func_' + p[1], [])
	else:
		p[0] = Node('func_' + p[1], [p[3]])


def p_cortege(p):
	"""cortege : formula
			   | cortege COMMA formula"""
	if len(p) == 2:
		p[0] = Node('cortege', [p[1]])
	else:
		p[0] = p[1].add_parts([p[3]])


# list в грамматике
def p_collection(p):
	"""collection : LSBRACKET RSBRACKET
			   | LSBRACKET cortege RSBRACKET"""
	p[0] = Node('collection', [])
	if len(p) == 4:
		p[0].add_parts([p[2]])


def p_for_structure(p):
	"""for_structure : FOR IDENTIFIER IN IDENTIFIER COLON NEWLINE block
					 | FOR IDENTIFIER IN RANGE LPAREN cortege RPAREN COLON NEWLINE block
					 | FOR IDENTIFIER IN collection COLON NEWLINE block"""
	if len(p) == 8:
		p[0] = Node('for_structure', [Node('value', [p[2]]), Node('in', [p[4]]), Node('body', [p[7]])])
	else:
		p[0] = Node('for_structure', [Node('value', [p[2]]), Node('range', [p[6]]), Node('body', [p[10]])])


def p_block(p):
	"""block : TAB zero_block
			 | TAB zero_block NEWLINE
			 | TAB zero_block NEWLINE block
			 | TAB zero_block NEWLINE block NEWLINE"""
	if len(p) <= 4:
		p[0] = Node('block', [p[2]])
	else:
		p[0] = p[4].shift_parts([p[2]])


def p_if_structure(p):
	"""if_structure : IF condition COLON NEWLINE block
					| IF condition COLON NEWLINE block ELSE COLON NEWLINE block
					| IF condition COLON NEWLINE block elif_structure
					| IF condition COLON NEWLINE block elif_structure ELSE COLON NEWLINE block"""
	ll = len(p)
	if ll == 6:
		p[0] = Node('if_structure', [Node('condition', [p[2]]), Node('body', [p[5]])])
	elif ll == 7:
		p[0] = Node('if_structure', [Node('condition', [p[2]]), Node('body', [p[5]]), p[6]])
	elif ll == 10:
		p[0] = Node('if_structure',
					[Node('condition', [p[2]]), Node('body', [p[5]]), Node('else_structure', [Node('body', [p[9]])])])
	elif ll == 11:
		p[0] = Node('if_structure',
					[Node('condition', [p[2]]), Node('body', [p[5]]), p[6],
					 Node('else_structure', [Node('body', [p[10]])])])


def p_elif_structure(p):
	"""elif_structure : ELIF condition COLON NEWLINE block
					  | ELIF condition COLON NEWLINE block elif_structure"""
	if len(p) == 6:
		p[0] = Node('elif_structure', [Node('elif', [Node('condition', [p[2]]), Node('body', [p[5]])])])
	else:
		p[0] = p[6].shift_parts([Node('elif', [Node('condition', [p[2]]), Node('body', [p[5]])])])


def p_condition(p):
	"""condition : formula COMPARISON formula
				 | LPAREN condition RPAREN
				 | NOT condition
				 | formula"""
	ll = len(p)
	if ll == 2:
		p[0] = p[1]
	elif ll == 3:
		# ????????
		pass
	elif ll == 4:
		if p.slice[2].type == 'COMPARISON':
			p[0] = Node(p[2], [p[1], p[3]])
		else:
			p[0] = p[2]


def p_while_structure(p):
	"""while_structure : WHILE condition COLON NEWLINE block"""
	p[0] = Node('WHILE', [Node('condition', [p[2]]), Node('body', [p[5]])])


def p_def_structure(p):
	"""def_structure : DEF IDENTIFIER LPAREN form_parameters RPAREN COLON NEWLINE block
					 | DEF IDENTIFIER LPAREN RPAREN COLON NEWLINE block"""
	ll = len(p)
	p[0] = Node('def_structure', [Node('name', [p[2]])])
	if ll == 8:
		p[0].add_parts([Node('parameters', []), Node('body', [p[7]])])
	elif ll == 9:
		p[0].add_parts([p[4], Node('body', [p[8]])])


def p_form_parameters(p):
	"""form_parameters : IDENTIFIER
					   | form_parameters COMMA IDENTIFIER"""
	if len(p) == 2:
		p[0] = Node('parameters', [p[1]])
	else:
		p[0] = p[1].add_parts([p[3]])


def p_return_structure(p):
	"""return_structure : RETURN formula
						| RETURN IDENTIFIER"""
	p[0] = Node('return_structure', [p[2]])


def p_error(p):
	print('Unexpected token:', p)


parser = yacc.yacc(debug=False)


def build_tree(code):
	return parser.parse(code)
