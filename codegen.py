# coding=utf8


def add(obj):
	return f"{gen(obj.parts[0].parts[0])} {obj.parts[0].type} {gen(obj.parts[0].parts[1])}"


def and_structure(obj):
	return f"{gen(obj.parts[0])} and {gen(obj.parts[1])}"


def assignment(obj):
	node = obj.parts[0]
	return f"{node.parts[0]} {node.type} {gen(node.parts[1])}\n"


def block(obj):
	return gen(obj.parts).replace('\n', '\n\t')


def body(obj):
	return gen(obj.parts[0])


def condition(obj):
	obj = obj.parts[0]
	return f"{gen(obj.parts[0])} {obj.type} {gen(obj.parts[1])}"


def cortege(obj):
	args = []
	for part in obj.parts:
		args.append(globals()[part.type](part))
	return ', '.join(args)


def collection(obj):
	return f"[{gen(obj.parts)}]"


def def_structure(obj):
	obj = obj.parts
	return f"""function {obj[0].parts[0]} ({', '.join(obj[1].parts)}) {{
	{block(obj[2].parts[0])}
}}\n"""


def deg(obj):
	return f"Math.pow({gen(obj.parts[0].parts[0])}, {gen(obj.parts[0].parts[1])})"


def elif_structure(obj):
	res = ""
	for part in obj.parts:
		res += f""" else if ({gen(part.parts[0])}) {{
	{block(part.parts[1].parts[0])}
}}"""
	return res


def else_structure(obj):
	res = f""" else {{
	{block(obj.parts[0].parts[0])}
}}\n"""
	return res


def for_structure(obj):
	if obj.parts[1].type == 'range':
		rng = obj.parts[1].parts[0].parts
		variable = obj.parts[0].parts[0]
		return f"""for (let {variable} = {gen(rng[0])}; {variable} < {gen(rng[1])}; {variable} += {gen(rng[2])}) {{
	{block(obj.parts[2])}
}}\n"""
	else:
		return f"""for ({obj.parts[0].parts[0]} in {gen(obj.parts[1].parts[0])}) {{
	{block(obj.parts[2])}
}}\n"""


def func_print(obj):
	return f"console.log({gen(obj.parts)})\n"


def func_range(obj):
	return range(int(gen(obj.parts[0].parts[0])), int(gen(obj.parts[0].parts[1])), int(gen(obj.parts[0].parts[2])))


def gen(obj):
	code = ""
	if hasattr(obj, '__iter__'):
		for part in obj:
			code += globals()[part.type](part)
	else:
		code += globals()[obj.type](obj)
	return code


def or_structure(obj):
	return f"{gen(obj.parts[0])} or {gen(obj.parts[1])}"


def IDENTIFIER(obj):
	return obj.parts[0]


def if_structure(obj):
	if_var = f"""if ({gen(obj.parts[0])}) {{
	{block(obj.parts[1].parts[0])}
}}"""
	if len(obj.parts) == 3:
		if_var += gen(obj.parts[2])
	elif len(obj.parts) == 4:
		if_var += gen(obj.parts[2])
		if_var += gen(obj.parts[3])
	else:
		if_var += '\n'
	return if_var


def INTEGER_NUMBER(obj):
	return obj.parts[0]


def mul(obj):
	return f"{gen(obj.parts[0].parts[0])} {obj.parts[0].type} {gen(obj.parts[0].parts[1])}"


def parents(obj):
	return f"({gen(obj.parts)})"


def return_structure(obj):
	return f"return {gen(obj.parts[0])}"


def STRING(obj):
	return obj.parts[0]
