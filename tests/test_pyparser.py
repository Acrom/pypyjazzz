import unittest
from pyparser import build_tree


class MyTestCase(unittest.TestCase):
	def test_assignment(self):
		# Код котороый проверяем
		input_file = open("assignment.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		# Структура которая должна получится
		output_file = open("assignment.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_while_structure(self):
		input_file = open("while_structure.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("while_structure.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_if_structure(self):
		input_file = open("if_structure.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("if_structure.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_for_structure(self):
		input_file = open("for_structure.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("for_structure.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_def_structure(self):
		input_file = open("def_structure.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("def_structure.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_arithmetic_operations(self):
		input_file = open("arithmetic_operations.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("arithmetic_operations.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_logical_operations(self):
		input_file = open("logical_operations.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("logical_operations.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_string(self):
		input_file = open("string.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("string.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

	def test_list(self):
		input_file = open("list.in.py", "r")
		data = input_file.read()
		input_file.close()

		result = build_tree(data)

		output_file = open("list.out", "r")
		data = output_file.read()
		output_file.close()

		self.assertEqual(str(result), str(data))

if __name__ == '__main__':
	unittest.main()
