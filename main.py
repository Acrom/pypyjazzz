# coding=utf8

from pyparser import build_tree
from codegen import gen

data = open("test1.py", "r").read()

result = build_tree(data)
print(result)
code = gen(result.parts)
print(code)
