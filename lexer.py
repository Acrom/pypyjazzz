# coding=utf8
import ply.lex as lex
from ply.lex import TOKEN
import re

reserved = {
	'if': 'IF',
	'elif': 'ELIF',
	'else': 'ELSE',
	'while': 'WHILE',
	'print': 'PRINT',
	'return': 'RETURN',
	'for': 'FOR',
	'def': 'DEF',
	'int': 'INT',
	'float': 'FLOAT',
	'str': 'STR',
	'len': 'LEN',
	'object': 'OBJECT',
	'abs': 'ABS',
	'round': 'ROUND',
	'ceil': 'CEIL',
	'floor': 'FLOOR',
	'list': 'LIST',  # уже занято, надо придумать название нормальное
	'range': 'RANGE',
	'in': 'IN',
	'not in': 'NOT_IN',
	'is': 'IS',
	'not is': 'NOT_IS',
}

tokens = [
			 'PROGRAM', 'ZERO_BLOCK', 'MODIFICATION', 'IDENTIFIER', 'WORD', 'CHARACTER', 'COMPARISON', 'ASSIGNMENT',
			 'ATOM',
			 'FORMULA', 'T', 'F', 'G', 'H', 'J', 'DIS', 'CON', 'NOT', 'ADD', 'MUL', 'DEG', 'NUMBER',
			 'INTEGER_NUMBER', 'DIGIT', 'RATIONAL_NUMBER', 'STRING', 'CHAIN', 'SYMBOL', 'BOOL', 'FUNCTION', 'CORTEGE',
			 'FOR_STRUCTURE', 'BLOCK', 'IF_STRUCTURE', 'ELIF_STRUCTURE', 'CONDITION',
			 'WHILE_STRUCTURE', 'DEF_STRUCTURE', 'FORM_PARAMETERS', 'LPAREN', 'RPAREN', 'NEWLINE', 'TAB', 'COMMA',
			 'COLON', 'LSBRACKET', 'RSBRACKET'
		 ] + list(reserved.values())

regex_separator = r'|'

t_DIGIT = r'([0-9])'
t_INTEGER_NUMBER = rf'{t_DIGIT}+'
t_RATIONAL_NUMBER = rf'{t_INTEGER_NUMBER}[.]{t_INTEGER_NUMBER}'


def t_DIS(t):
	r"""or"""
	return t


def t_CON(t):
	r"""and"""
	return t


def t_NOT(t):
	r"""not"""
	return t


def t_COMPARISON(t):
	r"""==|\!=|\>|\<|\>=|\<="""
	return t


def t_ASSIGNMENT(t):
	r"""=|\+=|-=|\*=|\/=|\%=|\&=|\|=|\<\<=|\>\>=|\*\*=|\/\/="""
	return t


def t_ADD(t):
	r"""\+|-"""
	return t


def t_DEG(t):
	r"""\*\*"""
	return t


def t_MUL(t):
	r"""\*|\/|\/\/|\%"""
	return t


ident = r'[a-zA-Z_]\w*'


t_STRING = r'""|"[^"]*"|\'\'|\'[^\']*\''
t_BOOL = r'True|False'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LSBRACKET = r'\['
t_RSBRACKET = r'\]'

t_TAB = r'\t'
t_COMMA = r','
t_COLON = r':'


@TOKEN(ident)
def t_IDENTIFIER(t):
	t.type = reserved.get(t.value, 'IDENTIFIER')
	return t


# здесь мы игнорируем незначащие символы. Нам ведь все равно, написано $var=$value или $var   =  $value
t_ignore = ' '


def t_NEWLINE(t):
	r"""\n"""
	t.lexer.lineno += len(t.value)
	return t


# а здесь мы обрабатываем ошибки. Кстати заметьте формат названия функции
def t_error(t):
	print("Illegal character '%s'" % t.value[0])
	t.lexer.skip(1)


lexer = lex.lex(reflags=re.UNICODE | re.DOTALL | re.IGNORECASE)

if __name__ == "__main__":
	data = open("test1.py", "r").read()

	lexer.input(data)

	while True:
		tok = lexer.token()  # читаем следующий токен
		if not tok:
			break  # закончились печеньки
		print(tok)
